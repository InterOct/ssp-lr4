﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LR4.Default" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>USD to BYN Exchange Rate</title>
    <link href="static/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<form class="container" id="form1" runat="server">
    <div class="row">
        <h1 class="text-center" style="font-size: 200%">USD to BYN Exchange Rate</h1>
    </div>
    <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="row">
                <asp:Timer ID="Timer" runat="server" Interval="1000" OnTick="Timer_Tick"></asp:Timer>
                <h2 class="text-center" style="font-family: Menlo, Monaco, Consolas, &quot;Courier New&quot;, monospace; font-size: 400%;">
                    <asp:Label ID="ExchangeRate" runat="server"></asp:Label>
                </h2>
                <h3 class="text-center" style="font-size: 100%">
                    Last update: <asp:Label ID="LastUpdate" runat="server"></asp:Label>
                </h3>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</form>
</body>
</html>