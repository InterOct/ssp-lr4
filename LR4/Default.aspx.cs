﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;

namespace LR4
{
    public partial class Default : Page
    {
        private static readonly Random Random = new Random();

        private static readonly Thread BackgroundThread = InitBackgroundThread();

        private static double CurrentRate = 1.874;
        private static double PreviousRate = 1.874;

        private static Thread InitBackgroundThread()
        {
            var thread = new Thread(() =>
            {
                while (Thread.CurrentThread.IsAlive)
                {
                    PreviousRate = CurrentRate;
                    CurrentRate = PreviousRate + GetRandomNumber(-0.1, 0.1);
                    Thread.Sleep(3000);
                }
            }) {IsBackground = true};
            thread.Start();
            return thread;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Timer_Tick(sender, e);
        }

        protected void Timer_Tick(object sender, EventArgs e)
        {
            string deltaText;
            var delta = CurrentRate - PreviousRate;
            if (delta >= 0)
                deltaText = $"<span class=\"text-success\">+{delta:F3}</span>";
            else
                deltaText = $"<span class=\"text-danger\">{delta:F3}</span>";
            ExchangeRate.Text = $"1 USD = {CurrentRate:F3} BYN ({deltaText})";
            LastUpdate.Text = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        }

        private static double GetRandomNumber(double minimum, double maximum)
        {
            return Random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}